import React, {Component} from 'react';
import {Button, Text, View} from 'react-native';
class Cat extends Component {
  state = {isHungry: true}; 
   render() {
    return (
      <View>
        <Text>I am {this.props.name}</Text>
        {this.state.isHungry ? (
          <Text>I am hungry</Text>
        ) : (
          <Text>I am full</Text>
        )}
        <Button
          onPress={() => {
            this.setState({isHungry: false});
          }}
          disabled={!this.state.isHungry}
          title={
            this.state.isHungry ? 'Pour me some milk, please!' : 'Thank you!'
          }
        />
      </View>
    );
  }
}export default Cat;