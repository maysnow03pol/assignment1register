import React, { Component } from 'react'
import {Image, View, Text, TouchableOpacity, TextInput, StyleSheet, ScrollView } from 'react-native';


class Inputs extends Component {
   state = {
      status:'',
      fullName : '',
      email: '',
      jobTitle: '',
      
      education: '',
      dateOfBirth:''
   }


   handledateOfBirth = (text) => {
      this.setState( {dateOfBirth: text })
   }

   handleEducation = (text) => {
      this.setState( {education: text })
   }

   handleFullName = (text) => {
      this.setState( {fullName: text })
   }

   handleEmail = (text) => {
      this.setState({email: text })
      
   }

   handleJobTitle = (text) => {
      this.setState( {jobTitle: text })
   }

   
   
   render() {
      if(this.state.status == "Filled")
      {
         return(
         <View style = {styles.display}>
            <ScrollView>
            <View style={styles.data}>
               <Text style = {styles.bold}>Full Name: </Text>
               <Text>{this.state.fullName}</Text>
            </View>
            <View style={styles.data}>
               <Text style = {styles.bold}>Email: </Text>
               <Text>{this.state.email}</Text>
            </View>
            <View style={styles.data}>
               <Text style = {styles.bold}>Job Title: </Text>
               <Text>{this.state.jobTitle}</Text>
            </View>
            <View style={styles.data}>
               <Text style = {styles.bold}>Education: </Text>
               <Text>{this.state.education}</Text>
            </View>
            <View style={styles.data}>
              <Text style = {styles.bold}> Date of Birth: </Text>
               <Text>{this.state.dateOfBirth}</Text>
            </View>
         </ScrollView>
         <View >
            <Image
              source={require('./image.png')}
              style={[
               
               {width: 90, height: 90 , resizeMode: 'contain',
               right:20,
               justifyContent:'flex-start'
               
               }
         
               
             ]}/>
         </View>
              
          
            

         </View>)
      }
      else
      {
         return (
            <ScrollView style = {styles.container}>
               
   
               <TextInput style = {styles.input}
                  underlineColorAndroid = "transparent"
                  placeholder = "Full Name"
                  placeholderTextColor = "#9a73ef"
                  autoCapitalize = "none"
                  onChangeText = {this.handleFullName}/>
               
               <TextInput style = {styles.input}
                  underlineColorAndroid = "transparent"
                  placeholder = "Email"
                  placeholderTextColor = "#9a73ef"
                  autoCapitalize = "none"
                  onChangeText = {this.handleEmail}/>
   
               <TextInput style = {styles.input}
                  underlineColorAndroid = "transparent"
                  placeholder = "Job Title"
                  placeholderTextColor = "#9a73ef"
                  autoCapitalize = "none"
                  onChangeText = {this.handleJobTitle}/>
   
               <TextInput style = {styles.input}
                  underlineColorAndroid = "transparent"
                  placeholder = "Education"
                  placeholderTextColor = "#9a73ef"
                  autoCapitalize = "none"
                  onChangeText = {this.handleEducation}/>
               
   
               
               
               <TextInput style = {styles.input}
                  underlineColorAndroid = "transparent"
                  placeholder = "Date Of Birth"
                  placeholderTextColor = "#9a73ef"
                  autoCapitalize = "none"
                  onChangeText = {this.handledateOfBirth}/>
               
               
               <TouchableOpacity
                  style={styles.saveButton}
                  onPress= {()=> {this.setState({status: 'Filled'})}}
               >
               <Text style={styles.saveButtonText}>Submit</Text>
               </TouchableOpacity>
               
   
                           
               
            </ScrollView>
         )
      }
      
   }
}

export default Inputs

const styles = StyleSheet.create({
   container: {
      paddingTop: 23
   },

   bold:{
      fontWeight:'bold',
   },

   data:{
      flex:1,
      flexDirection:'row'

   },

   display: {
      flex:1,
      flexDirection:'row'
   },

   
   input: {
      margin: 15,
      height: 40,
      borderColor: '#7a42f4',
      borderWidth: 1
   },
   saveButton: {
      backgroundColor: '#7a42f4',
      padding: 10,
      margin: 15,
      height: 40,
    },
    saveButtonText: {
      color: 'white',
      textAlign: 'center'
      
    }

 
    

})